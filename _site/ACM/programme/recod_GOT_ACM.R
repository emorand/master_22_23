
library(FactoMineR)

#import des données

perso_final <- read.csv("~/Dossier_tout_compris/Ined/Formation/master_2022_2023/Donnees/out/perso_final.csv", sep=";")





## programme plus classique 

library( tidyverse)

# select : verbe du tidy qui permet de selectionner les colonnes
# On enleve les colonnes à l'aide des `-`

perso<-select(perso_final, -Tue_par.y, -maison,-tueur)

## Recodage de perso_final$temps_presence en perso_final$temps_presence_rec
perso$presence <- cut(perso$temps_presence,
  include.lowest = TRUE,
  right = TRUE,
  dig.lab = 4,
  breaks = c(11, 142, 506, 41104)
)

perso$presence <- perso$presence %>%
  fct_recode(
    "provisoire" = "[11,142]",
    "recurrent" = "(142,506]",
    "principal" = "(506,4.11e+04]"
  )

perso<-select(perso, -temps_presence, -personnes_tue_n)

## On recode le premier et le dernier episode de présence en gardant la seance

perso <- mutate(perso, episode_ini =as.factor( paste("dep",paste(prem_saison,prem_episode,sep="."),sep=".")))
perso <- mutate(perso, episode_fin = as.factor(paste("fin",paste(dern_saison,dern_episode,sep="."),sep=".")))

perso<-select(perso, -dern_episode, -prem_episode)

# la dureé des premières et dernières scenes

perso$dern_duree_rec <- cut(perso$dern_duree,
                            include.lowest = TRUE,
                            right = TRUE,
                            dig.lab = 4,
                            breaks = c(2, 31, 82, 465)
)

perso$der_tempo <-as.factor(perso$dern_duree_rec) %>%
  fct_recode(
    "tres_courte_fin" = "[2,31]",
    "courte_fin" = "(31,82]",
    "longue_fin" = "(82,465]"
  )


perso<-mutate(perso,sortie=as.factor(case_when(
  prem_duree<dern_duree ~ "fin > entree",
  prem_duree==dern_duree~ "entree=sortie",
  prem_duree>dern_duree~ "entree>fin",
  TRUE ~ "raté" )))

  


## Recodage de perso$prem_duree en perso$prem_duree_rec
perso$prem_duree_rec <- cut(perso$prem_duree,
                            include.lowest = TRUE,
                            right = TRUE,
                            dig.lab = 4,
                            breaks = c(7, 57, 136, 661)
)
perso$prem_tempo <-as.factor(perso$prem_duree_rec) %>%
  fct_recode(
    "tres_courte_deb" = "[7,57]",
    "courte_deb" = "(57,136]",
    "longue_deb" = "(136,661]"
  )



perso<-select(perso, -dern_duree, -prem_duree)

# les scenes

perso$prem_scene_rec <- cut(perso$prem_scene,
                            include.lowest = TRUE,
                            right = TRUE,
                            dig.lab = 4,
                            breaks = c(1, 507, 1801, 3795)
)


perso$prem_scene_quali <-as.factor(perso$prem_scene_rec) %>%
  fct_recode(
    "scene_init_debut_serie" = "[1,507]",
    "scene_ini_milieur_serie" = "(507,1801]",
    "scene_ini_fin_serie" = "(1801,3795]"
  )


perso$dern_scene_rec <- cut(perso$dern_scene,
                            include.lowest = TRUE,
                            right = TRUE,
                            dig.lab = 4,
                            breaks = c(9, 1450, 2325, 3840)
)

perso$dern_scene_quali <-as.factor(perso$dern_scene_rec) %>%
  fct_recode(
    "init_scene_dern" = "[9,1450]",
    "milieu_scene_dern" = "(1450,2325]",
    "fin_scene_dern" = "(2325,3840]"
  )

perso<-select(perso, -dern_scene, -prem_scene,-dern_scene_rec ,-prem_scene_rec,prem_duree_rec,dern_duree_rec )


perso<-mutate(perso,premiere_saison=as.factor(case_when(
  prem_saison%in%c(1,2) ~ "entree_deb_serie",
  prem_saison%in%c(3,4,5)~ "entree_milieu_serie",
  prem_saison%in%c(6,7,8)~ "entree_fin_serie",
  TRUE ~ "raté" )))

perso<-mutate(perso,derniere_saison=as.factor(case_when(
  dern_saison%in%c(1,2) ~ "sortie_deb_serie",
  dern_saison%in%c(3,4,5)~ "sortie_milieu_serie",
  dern_saison%in%c(6,7,8)~ "sortie_fin_serie",
  TRUE ~ "raté" )))

perso<-select(perso, -dern_saison, -prem_saison,-prem_duree_rec, dern_duree_rec)



perso$nb_scene_rec <- cut(perso$nb_scene,
                          include.lowest = TRUE,
                          right = TRUE,
                          dig.lab = 4,
                          breaks = c(1, 2, 7, 632)
)


perso$nb_scene <-as.factor(perso$nb_scene_rec) %>%
  fct_recode(
    "peu_de_scene" = "[1,2]",
    "qq_scenes" = "(2,7]",
    "beaucoup_scene" = "(7,632]"
  )
perso<-select(perso ,-nb_scene_rec,-dern_duree_rec)




res.MCA<-MCA(perso[, -1],quali.sup=c(1,2,7,8),graph=FALSE)

# pour retravailler le graphique
# library(explor)
# explor(res.MCA)




# On décrit avec des personnages type
#perso[29,] 
# Samwell Tarly un pillier à droite mais sous (plutot ce qui peuvent disparaitre au debut, mais il est mal représenté sur l'axe 2'
#perso [580,] 
# typique du milieu sur l'axe a gauche' comme les soldats..

#perso[472,] 
# en haut à gauche

#perso[111,] 

#en bas à droite (bien représenté sur l'axe 2 pas sur le 1')

# On fait des profils pour les comparer

#which(colnames(perso)%in%c("episode_ini","episode_fin"))

tableau<-function(v1)
  
{

  tab<-length(unique(v1))
  
  return(tab)
}



tableau2<-function(v1,v2)
  
{
  
  tab<-(table(v1,v2))

  
  tab<-prop.table(tab,2) 

  
  return(tab)
}
retire<-c(1,2,3,8,9)
colnames(perso[,c(1,2,3,8,9)])
desc_tab<-perso[,-retire]
l1<-apply(desc_tab,2, FUN=tableau2,perso$premiere_saison)
l2<-apply(desc_tab,2, FUN=tableau)

nom_var<-rep(colnames(desc_tab),as.vector(l2))


profil<-data.frame(nom_var,do.call(rbind,l1))


profil<-data.frame(rownames(profil),profil[,c(1,3)])



l1<-apply(desc_tab,2, FUN=tableau2,perso$derniere_saison)
l2<-apply(desc_tab,2, FUN=tableau)

nom_var<-rep(colnames(desc_tab),as.vector(l2))


profil2<-data.frame(nom_var,do.call(rbind,l1))

profil2<-data.frame(rownames(profil2),profil2[,c(1,3)])



l1<-apply(desc_tab,2, FUN=tableau2,perso$der_tempo)
l2<-apply(desc_tab,2, FUN=tableau)

nom_var<-rep(colnames(desc_tab),as.vector(l2))


profil3<-data.frame(nom_var,do.call(rbind,l1))

profil3<-data.frame(rownames(profil3),profil3[,c(1,4)])


p3<-profil3 %>% group_by(nom_var) %>% slice(1)
p1<-profil %>% group_by(nom_var) %>% slice(1)
p2<-profil2 %>% group_by(nom_var) %>% slice(1)

# on represente les profils pour les comparer




